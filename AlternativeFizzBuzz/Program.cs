﻿using TwistedFizzBuzz.Models;
using TwistedFizzBuzz.Services.Implementation;

FizzBuzz fizzBuzz = new();
AlternativeFizzBuzz alternativeFizzBuzz = new()
{
    Tokens = new List<AlternativeToken>()
        {
            new AlternativeToken(5,"Fizz"),
            new AlternativeToken(9,"Buzz"),
            new AlternativeToken(27,"Bar")
        },
    Start = -20,
    End = 127
};
string answer = fizzBuzz.AlternativeFizzBuzz(alternativeFizzBuzz);
Console.WriteLine(answer);
