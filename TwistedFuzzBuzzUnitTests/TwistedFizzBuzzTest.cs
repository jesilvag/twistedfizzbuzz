using TwistedFizzBuzz.Models;
using TwistedFizzBuzz.Services.Implementation;

namespace TwistedFuzzBuzzUnitTests;

public class TwistedFizzBuzzTest
{
    private readonly FizzBuzz _fizzBuzz;
    public TwistedFizzBuzzTest()
    {
        _fizzBuzz = new FizzBuzz();
    }

    [Test]
    public void Should_AlternativeFizzBuzz_Return_Given_Word()
    {
        // Arrange
        AlternativeFizzBuzz fizzBuzz = new AlternativeFizzBuzz()
        {
            Start = 1,
            End = 5,
            Tokens = new List<AlternativeToken>()
            {
                new AlternativeToken(2,"Two"),
                new AlternativeToken(3,"Three")
            }
        };
        string expectedResponse = "1\nTwo\nThree\nTwo\n5\n";
        //Act
        string currentResponse = _fizzBuzz.AlternativeFizzBuzz(fizzBuzz);

        //Assert
        Assert.That(currentResponse, Is.EqualTo(expectedResponse));
    }

    [Test]
    public void Should_Return_Regular_FizzBuzz()
    {
        // Arrange
        int start = 1;
        int end = 5;
        string expectedResponse = "1\n2\nFizz\n4\nBuzz\n";
        //Act
        string currentResponse = _fizzBuzz.RegularFizzBuzz(start, end);

        //Assert
        Assert.That(currentResponse, Is.EqualTo(expectedResponse));
    }

    [Test]
    public void Should_Return_FizzBuzz_When_Number_Have_Both()
    {
        // Arrange
        int start = 14;
        int end = 16;
        string expectedResponse = "14\nFizzBuzz\n16\n";
        //Act
        string currentResponse = _fizzBuzz.RegularFizzBuzz(start, end);

        //Assert
        Assert.That(currentResponse, Is.EqualTo(expectedResponse));
    }

    [Test]
    public void Should_Return_Only_Numbers_When_Not_Multiples()
    {
        // Arrange
        List<int> numbers = new List<int>() {1,2,4,7,8,11 };
        string expectedResponse = "1\n2\n4\n7\n8\n11\n";
        //Act
        string currentResponse = _fizzBuzz.NonSequentialFizzBuzz(numbers);

        //Assert
        Assert.That(currentResponse, Is.EqualTo(expectedResponse));
    }
}