﻿using TwistedFizzBuzz.Models;

namespace TwistedFizzBuzz.Services.Interfaces;
public interface IFizzBuzz
{
    string RegularFizzBuzz(int start, int end);
    string NonSequentialFizzBuzz(List<int> numbers);
    string AlternativeFizzBuzz(AlternativeFizzBuzz alternativeFizzBuzz);
}
