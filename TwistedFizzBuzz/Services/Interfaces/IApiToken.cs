﻿using TwistedFizzBuzz.Models;

namespace TwistedFizzBuzz.Services.Interfaces;
public interface IApiToken
{

    Task<TokenResponse> GetTokenAsync();

    string GetFizzBuzz(TokenResponse tokenResponse, int start, int end);
}
