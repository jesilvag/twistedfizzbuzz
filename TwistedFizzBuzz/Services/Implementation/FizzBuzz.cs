﻿using System.Text;
using TwistedFizzBuzz.Models;
using TwistedFizzBuzz.Services.Interfaces;

namespace TwistedFizzBuzz.Services.Implementation;
public class FizzBuzz : FizzBuzzBase, IFizzBuzz
{
    public string AlternativeFizzBuzz(AlternativeFizzBuzz alternativeFizzBuzz)
    {
        StringBuilder response = new();
        for (int i = alternativeFizzBuzz.Start; i <= alternativeFizzBuzz.End; i++)
        {
            if (alternativeFizzBuzz.Tokens.Exists(x => i % x.Divisor == 0))
            {
                foreach (var item in alternativeFizzBuzz.Tokens)
                {
                    if (IsDivisibleBy(i, item.Divisor))
                    {
                        response.Append($"{item.Token}");
                    }
                }
                response.Append("\n");
            }
            else
            {
                response.Append($"{i}\n");
            }

        }
        return response.ToString();
    }

   public string NonSequentialFizzBuzz(List<int> numbers)
    {
        StringBuilder response = new();

        foreach (int i in numbers)
        {
            response.Append(RegularFizzBuzz(i));
        }
        return response.ToString();
    }

    public string RegularFizzBuzz(int start, int end)
    {
        StringBuilder response = new();
        for (int i = start; i <= end; i++)
        {
            response.Append(RegularFizzBuzz(i));
        }
        return response.ToString();
    }
}
