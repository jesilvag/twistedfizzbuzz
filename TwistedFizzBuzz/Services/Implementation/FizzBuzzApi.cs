﻿using Newtonsoft.Json;
using System.Text;
using TwistedFizzBuzz.Models;
using TwistedFizzBuzz.Services.Interfaces;

namespace TwistedFizzBuzz.Services.Implementation;
public class FizzBuzzApi : FizzBuzzBase, IApiToken
{
    const string BaseAddress = "https://rich-red-cocoon-veil.cyclic.app/random";

    public string GetFizzBuzz(TokenResponse tokenResponse, int start, int end)
    {
        StringBuilder sb = new();
        if (tokenResponse != null)
        {
            for (int i = start; i <= end; i++)
            {
                if (IsDivisibleBy(i, tokenResponse.Multiple))
                {
                    sb.Append($"{tokenResponse.Word}\n");
                }
            }
        }
        return sb.ToString();
    }

    public async Task<TokenResponse> GetTokenAsync()
    {
        TokenResponse tokenResponse = new();
        using (HttpClient client = new())
        {

            HttpResponseMessage response = await client.GetAsync(BaseAddress);

            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync();
                if (!string.IsNullOrEmpty(responseBody))
                {
                    tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(responseBody);
                }
            }
        }
        return tokenResponse;
    }
}
