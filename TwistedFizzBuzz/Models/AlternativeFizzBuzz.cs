﻿namespace TwistedFizzBuzz.Models;
public class AlternativeFizzBuzz
{
    public List<AlternativeToken> Tokens { get; set; } = new List<AlternativeToken>();
    public int Start { get; set; }
    public int End { get; set; }
}
