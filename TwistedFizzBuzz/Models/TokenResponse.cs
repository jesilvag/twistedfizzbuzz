﻿using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace TwistedFizzBuzz.Models;
public class TokenResponse
{
    [JsonProperty("multiple")]
    [JsonPropertyName("multiple")]
    public int Multiple { get; set; }

    [JsonProperty("word")]
    [JsonPropertyName("word")]
    public string Word { get; set; } = string.Empty;
}
