﻿namespace TwistedFizzBuzz.Models;
public class AlternativeToken
{
    public AlternativeToken(int divisor, string token)
    {
        Divisor = divisor;
        Token = token;
    }

    public int Divisor { get; set; }
    public string Token { get; set; }
}
