﻿using System.Text;

namespace TwistedFizzBuzz;
public class FizzBuzzBase
{
    protected bool IsDivisibleBy(int number, int divisor)
    {
        return number % divisor == 0;
    }

     protected string RegularFizzBuzz(int number)
    {
        if (IsDivisibleBy(number, 3) && IsDivisibleBy(number, 5))
        {
            return "FizzBuzz\n";
        }
        if (IsDivisibleBy(number, 3))
        {
            return "Fizz\n";
        }
        if (IsDivisibleBy(number, 5))
        {
            return "Buzz\n";
        }
        return $"{number}\n";
    }
}
